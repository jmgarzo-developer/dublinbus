
package com.jmgarzo.dublinbus;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import com.jmgarzo.dublinbus.data.DublinBusDBHelper;
import com.jmgarzo.dublinbus.sync.UpdateDbSyncUtils;
import com.jmgarzo.dublinbus.utilities.DBUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MyApplication extends Application {

    private static final String DATABASE_NAME = "DublinBus.db";
    private static String DB_PATH;
    private static final String LOG_TAG = MyApplication.class.getSimpleName();

    private static  Context mContext;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext =this.getApplicationContext();
        DB_PATH = this.getDatabasePath(DATABASE_NAME).toString();


        if (Build.VERSION.SDK_INT > 23 && !DublinBusDBHelper.checkDataBase()){
            copyDataBase();
        }
    }




    private int copyDataBase() {

        //Open your local db as the input stream
        try {
            InputStream myInput = this.getAssets().open(DATABASE_NAME);

            // Path to the just created empty db
            String outFileName = DB_PATH;

            //Open the empty db as the output stream
            OutputStream myOutput = new FileOutputStream(outFileName);

            //transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

            //Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
            return 0;
        } catch (IOException e) {

            Log.e(LOG_TAG, e.toString());
            return 1;
        }

    }


    public static Context getContext(){
        return mContext;
    }


//    private boolean checkDataBase() {
//
//
//        return isDatabaseExist();
//    }
//
//    private boolean isDatabaseExist() {
//        File dbFile = getApplicationContext().getDatabasePath(DB_PATH);
//        return dbFile.exists();
//    }
}