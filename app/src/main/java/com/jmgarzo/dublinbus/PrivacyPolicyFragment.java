package com.jmgarzo.dublinbus;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

/**
 * A placeholder fragment containing a simple view.
 */
public class PrivacyPolicyFragment extends Fragment {

    public PrivacyPolicyFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        getActivity().setTitle(getString(R.string.privacy_policy_title));

        WebView myWebView = (WebView) view.findViewById(R.id.webview);
        myWebView.loadUrl("https://github.com/jmgarzo/DublinBus/blob/master/PrivacyPolicy.md");

        return view;

    }
}
